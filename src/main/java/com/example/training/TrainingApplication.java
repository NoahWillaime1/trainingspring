package com.example.training;

import com.example.training.cake.entity.Cake;
import com.example.training.cake.repository.CakeRepository;
import com.example.training.person.entity.Person;
import com.example.training.person.repository.PersonRepository;
import com.example.training.user.entity.AppUser;
import com.example.training.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class TrainingApplication implements CommandLineRunner {
	private static final Logger logger = LoggerFactory.getLogger(TrainingApplication.class);
	@Value("${server.port}")
	private String serveurPort;
	private PersonRepository personRepository;
	private CakeRepository cakeRepository;
	private UserService userService;

	public TrainingApplication(PersonRepository personRepository, CakeRepository cakeRepository, UserService userService){
		this.personRepository =personRepository;
		this.cakeRepository = cakeRepository;
		this.userService = userService;
	}

	public static void main(String[] args) {
		SpringApplication.run(TrainingApplication.class, args);
	}

	@Override
	public void run(String... args) {
		Person person1 = new Person("John", "Wick");
		Person person2 = new Person("James", "Bond");
		Person person3 = new Person("Tom", "Nook");
		personRepository.save(person1);
		personRepository.save(person2);
		personRepository.save(person3);

		cakeRepository.save(new Cake("Fondant au chocolat", person1));
		cakeRepository.save(new Cake("Charlotte aux fraises", person1));
		cakeRepository.save(new Cake("NotALie", person2));
		logger.info("Database was created.");
		//
		userService.signUp(new AppUser("admin", "password"));
		logger.info("User was created, username: admin / password: password");
		//
		logger.info("Swagger documentation : http://localhost:"+ serveurPort+"/swagger-ui.html");
	}
}
