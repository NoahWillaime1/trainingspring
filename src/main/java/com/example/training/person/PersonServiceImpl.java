package com.example.training.person;

import com.example.training.exceptions.ObjetIntrouvableException;
import com.example.training.person.dto.PersonCreationDTO;
import com.example.training.person.entity.Person;
import com.example.training.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    private PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person createPerson(Person person) {
       personRepository.save(person);
       return person;
    }

    @Override
    public Person findOne(long id) {
        Person p = personRepository.findById(id);
        if (p == null) {
            throw new ObjetIntrouvableException("Person with id ("+id+") can't be found");
        }
        return p;
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public Person updateOne(Person person) {
        return personRepository.save(person);
    }

    @Override
    public int deleteOne(long id) {
        int deleteResp = personRepository.deleteById(id);
        if (deleteResp == 0) {
            throw new ObjetIntrouvableException("Person with id ("+id+") can't be found");
        }
        return deleteResp;
    }
}
