package com.example.training.person;

import com.example.training.person.dto.PersonCreationDTO;
import com.example.training.person.entity.Person;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PersonService {
    public Person createPerson(Person person);
    public Person findOne(long id);
    public List<Person> findAll();
    public Person updateOne(Person person);
    public int deleteOne(long id);
}
