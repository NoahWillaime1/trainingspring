package com.example.training.person;

import com.example.training.person.dto.PersonCreationDTO;
import com.example.training.person.dto.PersonUpdateDTO;
import com.example.training.person.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;

@RestController
@RequestMapping("/person")
public class PersonController {
    private PersonService personService;
    private PersonMapper personMapper;

    public PersonController(PersonService personService, PersonMapper personMapper) {
        this.personService = personService;
        this.personMapper = personMapper;
    }

    @GetMapping
    public ResponseEntity<Object> fetchAll() {
        return new ResponseEntity<>(personService.findAll(), HttpStatus.FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> fetchOne(@PathVariable("id") long id) {
        return new ResponseEntity<>(personService.findOne(id), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<Object> createOne(@Valid @RequestBody PersonCreationDTO createPersonDto) {
        Person p = personMapper.toPerson(createPersonDto);
        return new ResponseEntity<>(personService.createPerson(p), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modifyOne(@PathVariable("id") long id, @RequestBody PersonUpdateDTO personUpdateDTO) {
        Person p = personMapper.toPerson(personUpdateDTO);
        p.setId(id);
        return new ResponseEntity<>(personService.updateOne(p), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> deleteOne(@PathVariable("id") long id) {
        return new ResponseEntity<>(personService.deleteOne(id), HttpStatus.OK);
    }
}
