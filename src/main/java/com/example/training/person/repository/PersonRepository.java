package com.example.training.person.repository;

import com.example.training.person.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    Person findById(long id);

    @Transactional /* Pour une exception JPA, No EntityManager with actual transaction available */
    int deleteById(long id);
}
