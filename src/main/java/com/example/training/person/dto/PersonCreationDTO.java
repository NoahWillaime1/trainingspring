package com.example.training.person.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class PersonCreationDTO {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    private Set<Long> cakesIds;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Long> getCakesIds() {
        return cakesIds;
    }

    public void setCakesIds(Set<Long> cakesIds) {
        this.cakesIds = cakesIds;
    }
}
