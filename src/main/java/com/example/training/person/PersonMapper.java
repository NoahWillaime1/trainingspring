package com.example.training.person;

import com.example.training.cake.CakeService;
import com.example.training.person.dto.PersonCreationDTO;
import com.example.training.person.dto.PersonUpdateDTO;
import com.example.training.person.entity.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CakeService.class})
public interface PersonMapper {

    @Mapping(source = "cakesIds", target = "cakes")
    Person toPerson(PersonCreationDTO creationDTO);
    @Mapping(source = "cakesIds", target = "cakes")
    Person toPerson(PersonUpdateDTO updateDTO);
}
