package com.example.training.cake;

import com.example.training.cake.entity.Cake;

import java.util.List;
import java.util.Set;

public interface CakeService {
    public List<Cake> findAll();
    public Set<Cake> findSome(Set<Long> cakesIds);
    public Cake findOne(long id);
    public Cake createOne(Cake cake);
    public Cake updateOne(Cake cake);
    public int deleteOne(long id);
}
