package com.example.training.cake.dto;

import javax.validation.constraints.NotNull;

public class CakeUpdateDTO {
    private String name;
    @NotNull
    private long ownerId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}
