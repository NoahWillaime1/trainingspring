package com.example.training.cake.dto;

import com.example.training.person.entity.Person;

import javax.validation.constraints.NotNull;

public class CakeCreationDTO {
    @NotNull
    private String name;

    @NotNull
    private long ownerId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}
