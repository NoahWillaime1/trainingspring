package com.example.training.cake;

import com.example.training.cake.entity.Cake;
import com.example.training.cake.repository.CakeRepository;
import com.example.training.exceptions.ObjetIntrouvableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CakeServiceImpl implements CakeService {
    private CakeRepository cakeRepository;

    public CakeServiceImpl(CakeRepository cakeRepository) {
        this.cakeRepository = cakeRepository;
    }

    @Override
    public List<Cake> findAll() {
        return cakeRepository.findAll();
    }

    @Override
    public Set<Cake> findSome(Set<Long> cakesIds) {
        List<Long> ids = new ArrayList<>(cakesIds);
        List<Cake> cakes =  cakeRepository.findByIdIn(ids);
        return new HashSet<>(cakes);
    }

    @Override
    public Cake findOne(long id) {
        Cake c = cakeRepository.findById(id);
        if (c == null) {
            throw new ObjetIntrouvableException("Can't find CAKE with id ("+id+"), is the cake a lie ?");
        }
        return c;
    }

    @Override
    public Cake createOne(Cake cake) {
        return cakeRepository.save(cake);
    }

    @Override
    public Cake updateOne(Cake cake) {
        return cakeRepository.save(cake);
    }

    @Override
    public int deleteOne(long id) {
        int deleteResp = cakeRepository.deleteById(id);
        if (deleteResp == 0) {
            throw new ObjetIntrouvableException("Can't find CAKE with id ("+id+"), is the cake a lie ?");
        }
        return deleteResp;
    }
}
