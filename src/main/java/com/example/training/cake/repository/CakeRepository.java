package com.example.training.cake.repository;

import com.example.training.cake.entity.Cake;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface CakeRepository extends JpaRepository<Cake, Integer> {
    Cake findById(long id);
    List<Cake> findByIdIn(List<Long> ids);
    @Transactional
    int deleteById(long id);
}
