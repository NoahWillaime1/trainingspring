package com.example.training.cake;

import com.example.training.cake.dto.CakeCreationDTO;
import com.example.training.cake.dto.CakeUpdateDTO;
import com.example.training.cake.entity.Cake;
import com.example.training.person.PersonService;
import com.example.training.person.entity.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {PersonService.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CakeMapper {

    @Mapping(source = "ownerId", target = "owner")
    Cake toEntity(CakeCreationDTO cakeCreationDTO);

    @Mapping(source = "ownerId", target = "owner")
    Cake toEntity(CakeUpdateDTO cakeUpdateDTO);
}
