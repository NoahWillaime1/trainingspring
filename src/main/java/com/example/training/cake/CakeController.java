package com.example.training.cake;

import com.example.training.cake.dto.CakeCreationDTO;
import com.example.training.cake.dto.CakeUpdateDTO;
import com.example.training.cake.entity.Cake;
import com.example.training.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping("/cake")
public class CakeController {
    private CakeMapper cakeMapper;
    private CakeService cakeService;

    public CakeController(CakeService cakeService, CakeMapper cakeMapper){
        this.cakeService = cakeService;
        this.cakeMapper = cakeMapper;
    }

    @GetMapping
    public ResponseEntity<List<Cake>> fetchAll() {
        return new ResponseEntity<>(cakeService.findAll(), HttpStatus.FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cake> fetchOne(@PathVariable("id") long id) {
        return new ResponseEntity<>(cakeService.findOne(id), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<Cake> createOne(@Valid @RequestBody CakeCreationDTO cakeCreationDTO) {
        Cake c = cakeMapper.toEntity(cakeCreationDTO);
        return new ResponseEntity<>(cakeService.createOne(c), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Cake> updateOne(@PathVariable("id") long id, @RequestBody CakeUpdateDTO cakeUpdateDTO) {
        Cake c = cakeMapper.toEntity(cakeUpdateDTO);
        c.setId(id);
        return new ResponseEntity<>(cakeService.updateOne(c), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> deleteOne(@PathVariable("id") long id) {
        return new ResponseEntity<>(cakeService.deleteOne(id), HttpStatus.OK);
    }
}
