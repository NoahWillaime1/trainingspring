package com.example.training.user.service;

import com.example.training.user.entity.AppUser;

import java.util.List;

public interface UserService {
    public List<AppUser> findAll();
    public AppUser signUp(AppUser user);
}
