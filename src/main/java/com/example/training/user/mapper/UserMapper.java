package com.example.training.user.mapper;

import com.example.training.user.dto.UserCreationDTO;
import com.example.training.user.entity.AppUser;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    AppUser dtoToEntity(UserCreationDTO userCreationDTO);
}
