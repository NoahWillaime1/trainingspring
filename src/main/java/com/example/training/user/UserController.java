package com.example.training.user;

import com.example.training.user.dto.UserCreationDTO;
import com.example.training.user.entity.AppUser;
import com.example.training.user.mapper.UserMapper;
import com.example.training.user.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    private UserService userService;
    private UserMapper userMapper;


    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping
    public ResponseEntity<List<AppUser>> fetchAll() {
        return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<AppUser> createOne(@RequestBody UserCreationDTO userCreationDTO) {
        AppUser user = userMapper.dtoToEntity(userCreationDTO);
        return new ResponseEntity<>(userService.signUp(user), HttpStatus.OK);
    }

}
