package com.example.training.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ObjetIntrouvableException extends RuntimeException {
    public ObjetIntrouvableException(String msg) {
        super (msg);
    }
}
