package com.example.training;

import com.example.training.user.dto.UserCreationDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ToLogController {
    @ApiOperation("Login.")
    @PostMapping("/login")
    public void fakeLogin(@RequestBody UserCreationDTO userCreationDTO) {
        throw new IllegalStateException("This method shouldn't be called. It's implemented by Spring Security filters.");
    }
}
